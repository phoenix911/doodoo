const util = require("../../../utils/util.js");
Page({
    data: {
        Loading: 0,
        color: wx.getExtConfigSync().color,
        isLoadedAll: false,
        collectionList: [],
        page: 1
    },
    onLoad() {},
    onShow() {
        this.collection(this.data.page);
    },
    // 获取收藏的商品和店铺列表
    collection(page) {
        wx.doodoo.fetch(`/shop/api/shop/collection/index?page=${page}`).then(res => {
            if (res && res.data.errmsg == 'ok' && res.data.data && res.data.data.data.length) {
                this.data.collectionList = res.data.data.data;
                let _pagination = res.data.data.pagination;
                let _data = res.data.data.data;
                let obj = {};
                if (_pagination.pageCount == 1) {
                    this.data.collectionList = _data;
                    this.data.isLoadedAll = true; //已经全部加载完毕
                } else {
                    if (_pagination.page < _pagination.pageCount) {
                        if (this.data.collectionList.length) {
                            if (JSON.stringify(this.data.collectionList).indexOf(JSON.stringify(_data[0])) == -1) {
                                this.data.collectionList.push.apply(this.data.collectionList, _data); //数组合并
                            }
                        } else {
                            this.data.collectionList.push.apply(this.data.collectionList, _data); //数组合并
                        }
                    } else if (_pagination.page == _pagination.pageCount) {
                        if (!this.data.isLoadedAll) {
                            this.data.collectionList.push.apply(this.data.collectionList, _data); //数组合并
                            this.data.isLoadedAll = true; //已经全部加载完毕
                        }
                    } else {
                        this.data.isLoadedAll = true; //已经全部加载完毕
                    }
                }
                this.setData({
                    Loading: 1,
                    collectionList: this.data.collectionList
                });
            } else {
                this.setData({
                    Loading: 1,
                    collectionList: []
                });
            }
        });
    },
    goProductDetail(e) {
        wx.navigateTo({
            url: `/pages/shop/product/product-detail/index?id=${e.currentTarget.dataset.id}`
        });
    },
    onReachBottom: function() {
        if (!this.data.isLoadedAll) {
            this.data.page++;
            this.collection(this.data.page);
        }
    },
    onPullDownRefresh: function() {
        wx.stopPullDownRefresh();
        this.data.page = 1;
        this.data.collectionList = [];
        this.collection(this.data.page);
    }
});