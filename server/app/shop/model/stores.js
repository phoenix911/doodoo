module.exports = doodoo.bookshelf.Model.extend({
    tableName: "stores",
    hasTimestamps: true,
    softDelete: true
});
