const product = require("./product");

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "shop_order_detail",
    hasTimestamps: true,
    product: function() {
        return this.belongsTo(product, "product_id");
    }
});
